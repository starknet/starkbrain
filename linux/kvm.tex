% Base du document
\documentclass[a4paper, 11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}

% Pour la table des matières, les sources et les liens Web
\usepackage{hyperref}

% Puces par point
\usepackage{enumitem}
\setlist[itemize]{label=\textbullet}

% Mettre en valeur les commandes
\usepackage{listings}
\usepackage{xcolor}

% Tableau des commandes KVM
\usepackage{tabularx}

% Configuration du style d'affichage des commandes
\definecolor{lightgray}{rgb}{0.8,0.8,0.8}

\lstdefinestyle{customshell}{
  language=bash,
  backgroundcolor=\color{lightgray},
  basicstyle=\color{black}\ttfamily,
  keywordstyle=\ttfamily,
  commentstyle=\color{purple}\ttfamily,
  stringstyle=\color{purple}\ttfamily,
  showstringspaces=false,
  frame=none,
  breaklines=true
}

% Suppression de l'alinéa
\setlength{\parindent}{0pt}

\title{Installer et configurer KVM sur Debian 11}
\author{StarkNet}
\date{\today}

\begin{document}

\maketitle
\thispagestyle{empty}

\begin{abstract}
  Ceci est un tutoriel rédigé en \LaTeX pour vous apprendre à
  mettre en place et à utiliser l'hyperviseur KVM sous Debian
  GNU/Linux Bullseye. Si vous souhaitez une configuration plus
  avancée, veuillez lire attentivement le
  \href{https://wiki.debian.org/KVM}{Wiki Debian} sur le sujet.
\end{abstract}

\newpage
  \tableofcontents
\newpage

\section{Introduction}

\subsection{KVM}

KVM (Kernel-based Virtual Machine) est une technologie de
virtualisation libre qui transforme le noyau Linux en un
hyperviseur de machine virtuelle. Il permet d'exécuter
plusieurs machines virtuelles sur une seule machine
physique, offrant une isolation complète et de
bonnes performances.

\subsection{QEMU}

QEMU (Quick EMUlator) est un émulateur et un outil de
virtualisation polyvalent. Il permet d'exécuter des
systèmes d'exploitation invités, même sur des
architectures différentes de celle de l'hôte.
QEMU peut également être utilisé avec KVM
pour offrir une virtualisation complète
et flexible.

\subsection{Avantages}

En utilisant ses technologies, vous pouvez créer et gérer
efficacement des machines virtuelles, en exécutant différents
systèmes d'exploitation invités sur une seule machine physique.
Cela offre des avantages tels que l'isolation des environnements,
la consolidation des ressources et la flexibilité des configurations.

\section{Prérequis}

\begin{itemize}
  \item Une machine prenant en charge le support de virtualisation
       (\href{https://en.wikipedia.org/wiki/X86_virtualization#Intel_virtualization_(VT-x)}{Intel VT-x} ou
        \href{https://en.wikipedia.org/wiki/X86_virtualization#AMD_virtualization_(AMD-V)}{AMD-V} activé)
  \item Un système Debian GNU/Linux 11
  \item Un utilisateur sudoer
  \item Un accès Internet
\end{itemize}

\section{Confirmer le support de virtualisation}

\subsection{BIOS/UEFI}

Tout d'abord, vous devez avoir un processeur Intel ou AMD avec
\textbf{l'option de virtualisation activée} dans votre BIOS
ou UEFI. Parcourez les menus à la recherche des options
  << Intel Virtualisation Technology >>,
  << Technologie de virtualisation Intel >>,
  << Intel VT-x >>,
  << AMD-V >>,
  << Secure Virtual Machine Mode>> ou encore
  << SVM Mode >>.
N'hésitez pas à vous référer à la documentation de votre
carte mère pour trouver ce paramètre.

\subsection{Vérifier sur le système}

Pour confirmer que l'option est bien activée, exécutez
les commandes suivantes sur votre système Debian.

\begin{lstlisting}[style=customshell]
  egrep -c '(vmx|svm)' /proc/cpuinfo
  grep -E --color '(vmx|svm)' /proc/cpuinfo
\end{lstlisting}

Si la sortie de la première commande est \textbf{supérieure à zéro},
cela signifie que votre processeur prend en charge la virtualisation
et que ce paramètre est donc activé. Le nombre indiqué signifie
combien de coeurs de votre processeur prennent en charge la
virtualisation matérielle. Si ce n'est pas le cas, veuillez
vérifier que \textbf{l'option de virtualisation} est bien
activée comme vu précédemment. Enfin, la dernière
commande permet d'obtenir des informations sur
votre CPU. Si le texte de sortie de couleur
rouge est
\textbf{ <<VMX >>}, il s'agit d'Intel, alors que
\textbf{<< SVM >>} signifie AMD.

\section{Installation}

\subsection{Mise à jour}

Avant toute chose, exécutez la commande de mise à jour du système
si cela n'a pas déjà été fait pour mettre à jour les paquets
et reconstruire le cache du référentiel.

\begin{lstlisting}[style=customshell]
  sudo apt update
\end{lstlisting}

\subsection{Ajout des paquets}

Maintenant, nous pouvons installer les paquets requis pour
mettre en place notre environnement de virtualisation,
c'est-à-dire les paquets pour l'hyperviseur
\textbf{KVM},
\textbf{QEMU} et la bibliothèque
\textbf{libvirt}.

\begin{lstlisting}[style=customshell]
  sudo apt install qemu-kvm libvirt-clients \
                   libvirt-daemon-system \
                   bridge-utils virtinst \
                   libvirt-daemon
\end{lstlisting}

Ceux qui utilisent un environnement graphique sur le système
peuvent également installer \textbf{Virt-Manager}, une interface
graphique développée par RedHat pour créer, gérer et exécuter des
machines virtuelles utilisant la technologie KVM.

\begin{lstlisting}[style=customshell]
  sudo apt install virt-manager
\end{lstlisting}

\section{Configuration réseau}

\subsection{Réseau actif et démarrage automatique}

Pour répertorier les informations sur le réseau virtuel à utiliser
dans les machines virtuelles, exécutez la commande suivante.

\begin{lstlisting}[style=customshell]
  sudo virsh net-list --all
\end{lstlisting}

En effet, vous verrez que le réseau par défaut n'est pas actif et
qu'il n'est pas configuré pour démarrer automatiquement. Par
conséquent, pour le rendre actif et démarrer automatiquement,
lancez les deux commandes suivantes.

\begin{lstlisting}[style=customshell]
  sudo virsh net-start default
  sudo virsh net-autostart default
\end{lstlisting}

En vérifiant de nouveau, voici la nouvelle sortie.

\begin{lstlisting}[style=customshell]
   Name      State    Autostart   Persistent
  --------------------------------------------
   default   active   yes         yes
\end{lstlisting}

\subsection{Pilote vhost-net}

\subsubsection{Explication}

\textbf{vhost-net} est un pilote de périphérique virtuel qui permet
une communication optimisée entre les machines virtuelles et l'hôte
en réduisant les surcharges liées à la virtualisation réseau.
Ce pilote est utilisé en conjonction avec le pilote
\textbf{virtio-net}. vhost-net est le backend (côté hôte)
tandis que virtio-net (côté invité) est l'interface
qui s'exécute dans l'espace du noyau invité.

\subsubsection{Intégration}

Maintenant, ajoutons ce pilote via le gestionnaire de modules
\textbf{modprobe} et vérifions son activation.

\begin{lstlisting}[style=customshell]
  sudo modprobe vhost_net # Ajout du pilote
  lsmod | grep vhost      # Confirmer son activation
\end{lstlisting}

\section{Création des machines virtuelles}

Dans cette partie, nous allons traiter uniquement la création des
machines virtuelles par la ligne de commande avec \textbf{Virt}.
Vous pouvez aussi bien le faire par l'interface graphique via
\textbf{Virt-Manager} si vous ne voulez pas passer par la
console.

\subsection{Créer un disque virtuel}

Tout d'abord, nous devons créer une image disque pour notre
nouvelle machine virtuelle. Ici, nous allons créer une machine
virtuelle \textbf{Debian 11} sans environnement graphique et donc
lui allouer une taille suffisante de \textbf{10 gigaoctets}.

\begin{lstlisting}[style=customshell]
  qemu-img create -f qcow2 /var/lib/libvirt/images/debian.qcow2 10G
\end{lstlisting}

\subsection{Configurer et installer}

Nous allons maintenant procéder à la configuration et à
l'installation de la machine virtuelle. Tout d'abord, il
faudra bien entendu avoir téléchargé le fichier ISO
\href{https://www.debian.org/download}{Debian 11}
sur le site officiel de la fondation.

\begin{lstlisting}[style=customshell]
  sudo virt-install \
    --virt-type kvm \
    --name debian \
    --ram 1024 \
    --disk /var/lib/libvirt/images/debian.qcow2 \
    --network network=default \
    --graphics vnc,listen=0.0.0.0 \
    --noautoconsole \
    --os-type=linux \
    --os-variant=generic \
    --cdrom=~/Downloads/debian-11.7.0-amd64-netinst.iso
\end{lstlisting}

Vous pouvez bien entendu changer le \textbf{nom} ou la quantité
de \textbf{mémoire} selon vos besoins.

\begin{itemize}
  \item \textbf{-{}-virt-type} :     Le type de virtualisation à
                                     utiliser (ici KVM) ;
  \item \textbf{-{}-name} :          Le nom de la machine virtuelle
                                     (debian) ;
  \item \textbf{-{}-ram} :           La quantité de mémoire allouée
                                     en mégaoctet (1024 Mo) ;
  \item \textbf{-{}-disk} :          Le chemin du disque que nous
                                     avons créé à l'aide de QEMU ;
  \item \textbf{-{}-network} :       Spécifie la configuration réseau
                                     pour la machine virtuelle, en
                                     utilisant le réseau par défaut ;
  \item \textbf{-{}-graphics} :      Définit la méthode de sortie
                                     graphique pour la machine virtuelle,
                                     utilisant le protocole VNC et
                                     écoutant sur toutes les interfaces
                                     (0.0.0.0) ;
  \item \textbf{-{}-noautoconsole} : Désactive la console automatique
                                     de la machine virtuelle lors
                                     du démarrage ;
  \item \textbf{-{}-os-type} :       Indique le type de système d'exploitation
                                     de la machine virtuelle, dans ce cas
                                     GNU/Linux ;
  \item \textbf{-{}-os-variant} :    Spécifie la variante du système
                                     d'exploitation, dans ce cas
                                     une variante générique ;
  \item \textbf{-{}-cdrom} :         Indique le chemin vers le fichier ISO
                                     pour l'installation de Debian 11.
\end{itemize}

\subsection{Afficher l'interface graphique}

\subsubsection{VNC Viewer}

Pour afficher l'interface graphique de la machine virtuelle, vous
pouvez utiliser VNC Viewer tel que le client \textbf{TigerVNC}.

\begin{lstlisting}[style=customshell]
  sudo apt install tigervnc-viewer
\end{lstlisting}

Ensuite, vérifiez le numéro de \textbf{port d'affichage} VNC
pour afficher la VM Debian en spécifiant son nom.

\begin{lstlisting}[style=customshell]
  sudo virsh vncdisplay debian
\end{lstlisting}

Ouvrez maintenant TigerVNC (\textbf{xtigervncviewer}) et taper
l'adresse IP de votre hôte sur lequel KVM a été installé avec
le port.

\subsubsection{Connaître l'adresse IP}

Pour connaître l'adresse IP en question, si vous n'avez pas
fait de paramétrage particulier sur la carte réseau réservée
à KVM.

\begin{lstlisting}[style=customshell]
  ip -4 a
\end{lstlisting}

Et recherchez la ligne \textbf{virbr0}. Voici un extrait d'une
sortie possible.

\begin{lstlisting}[style=customshell]
  1: lo: [...]
  2: enp4s0: [...]
  3: wlp3s0: [...]
  4: virbr0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default qlen 1000
      inet 192.168.122.1/24 brd 192.168.122.255 scope global virbr0
         valid_lft forever preferred_lft forever
\end{lstlisting}

Dans cet exemple, l'adresse IP de la VM Debian est donc
\textbf{\texttt{192.168.122.1}}. Ainsi, je dois saisir dans
TigerVNC l'adresse IP et le port d'affichage comme vu précédemment
\textbf{\texttt{192.168.122.1:<portVNC>}}.

\subsection{Commandes basiques}

\begin{tabularx}{\linewidth}{|l|X|}
  \hline
  virsh list -{}-all                         & Voir les VM
                                               installées
                                               \\
  \hline
  virsh stop \texttt{<nom\_vm>}              & Arrête une VM
                                              (arrêt immédiat)
                                               \\
  \hline
  virsh start \texttt{<nom\_vm>}             & Démarre une VM
                                               \\
  \hline
  virsh save \texttt{<nom\_vm> <sauvegarde>} & Sauvegarde l'état
                                               d'une VM dans un
                                               fichier
                                               \\
  \hline
  virsh restore \texttt{<sauvegarde>}        & Restaure l'état
                                               sauvegardé d'une VM
                                               \\
  \hline
  virsh reboot \texttt{<nom\_vm>}            & Redémarre une VM
                                               \\
  \hline
  virsh suspend \texttt{<nom\_vm>}           & Suspends une VM
                                               \\
  \hline
  virsh resume \texttt{<nom\_vm>}            & Reprends l'exécution
                                               d'une VM après sa
                                               suspension
                                               \\
  \hline
  virsh shutdown \texttt{<nom\_vm>}          & Arrête une VM
                                              (signal d'arrêt)
                                               \\
  \hline
  virsh destroy \texttt{<nom\_vm>}           & Arrête brutalement
                                               une VM
                                              (similaire à une
                                               coupure de courant)
                                               \\
  \hline
  virsh console \texttt{<nom\_vm>}           & Se connecter à la
                                               console d'une VM
                                               \\
  \hline
  virsh undefine \texttt{<nom\_vm>}          & Désindexe une VM de
                                               la liste des VM
                                               installés
                                              (suppression manuelle
                                               du disque requise)
                                               \\
  \hline
\end{tabularx}

\newpage\section{Sources}

\begin{itemize}
  \item \href{https://orbilu.uni.lu/bitstream/10993/16701/1/Tutorial_Latex.pdf}{Tutorial \LaTeX}
  \item \href{https://www.linux-kvm.org/page/Documents}{Documents KVM}
  \item \href{https://fr.wikipedia.org/wiki/Kernel-based_Virtual_Machine}{Kernel-based Virtual Machine - Wikipédia}
  \item \href{https://fr.wikipedia.org/wiki/QEMU}{QEMU - Wikipédia}
  \item \href{https://linux.how2shout.com/how-to-install-and-configure-kvm-on-debian-11-bullseye-linux/}{How to Install and Configure KVM on Debian 11 Bullseye Linux}
  \item \href{https://linux.how2shout.com/how-to-install-kvm-and-virt-manager-on-rocky-linux-8/#4_Command-line_to_create_a_KVM_virtual_machine}{Command-line to create a KVM virtual machine}
  \item \href{https://lecrabeinfo.net/activer-la-virtualisation-intel-vt-x-amd-v-dans-le-bios-uefi.html#activer-la-virtualisation-intel-vt-x-amd-v-dans-le-biosuefi}{Activer la virtualisation (Intel VT-x / AMD-V) dans le BIOS/UEFI}
\end{itemize}

\end{document}
