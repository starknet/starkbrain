# StarkBrain

# Introduction

Dans notre monde numérique en constante évolution, l'accès à
l'information et la capacité à partager des connaissances sont
des éléments clés pour favoriser l'apprentissage et la croissance.
Ce projet vise à créer un espace de partage de savoir en informatique
où les passionnés de tous les niveaux pourront en apprendre davantage.
L'ensemble des documents sont soumis à la
  [Licence publique générale GNU version 3 ou ultérieure](https://www.gnu.org/licenses/gpl-3.0.html).
Partagez et élargissez vos horizons !

# Consulter les documents

Tous les documents sont fabriqués en **LaTeX**. Pour pouvoir les
compiler et les visualiser, il sera nécessaire d'installer les
différents composants LaTeX sur votre système. Vous allez
également avoir besoin de l'utilitaire de compilation **Make**.
Celui-ci est la plupart du temps préinstallé sur les systèmes
basés sur UNIX/Linux. Le cas échéant, vous devrez l'intégrer
en utilisant les outils de gestion de paquets de votre système.

## Installation

### Debian

Pour les systèmes **Debian**, vous devez installer les paquets
suivants pour permettre la compilation des documents.

* Installation minimale : [texlive](https://packages.debian.org/bullseye/texlive)
* Support du français   : [texlive-lang-french](https://packages.debian.org/bullseye/texlive-lang-french)
* Greffons utiles       : [texlive-latex-extra](https://packages.debian.org/bullseye/texlive-latex-extra)

Pour cela rien de plus simple, il suffit de lancer la commande
`make` suivie de la cible d'installation avec les privilèges
**super-utilisateur**.

```shell
make install
```

Le paquet [make](https://packages.debian.org/bullseye/make)
est normalement présent par défaut sur ce système.
A noter que macOS l'intègre également.

### Systèmes non GNU/Linux

Pour cet univers, nous avons à disposition
  [MacTeX](https://www.tug.org/mactex/mactex-download.html) pour macOS, ainsi que
  [MiKTeX](https://miktex.org/download) couplé à
  [MSYS2](https://www.msys2.org/) pour l'intégration de Make dans Windows.
Consultez le site officiel du
  [projet LaTeX](https://www.latex-project.org) pour plus d'informations.

## Compilation

Afin de compiler tous les documents LaTeX, utilisez simplement la
commande `make` sans option à la racine du projet.

```shell
make
```

Cela vous sortira tous les documents en **PDF** que vous
pourrez consulter sur n'importe quel appareil.

Voici les différentes cibles possibles pour cette commande.

```shell
make install   # Installe les paquets LaTeX requis
make uninstall # Désinstalle les paquets LaTeX

make [all]     # Génère les fichiers PDF et auxiliaires
make pdf       # Ne génère que les fichiers PDF

make clean     # Supprime tous les fichiers générés
make clean-aux # Supprime tous les fichiers générés, sauf les PDF
```

## Mise à jour

Si vous avez cloné le projet avec Git, utilisez simplement la commande
`git pull` dans votre copie de travail pour mettre à jour les
fichiers sources.

```shell
git pull
```

A noter que vous devez **recompiler** les documents avec Make pour
actualiser les PDF.

# Les documents

* [cisco.tex](https://gitlab.com/starknet/starkbrain/-/blob/main/cisco.tex) : Mémo des commandes utiles sur la configuration des actifs Cisco

## GNU/Linux

* [cli.tex](https://gitlab.com/starknet/starkbrain/-/blob/main/linux/cli.tex) : Mémo des commandes utiles GNU/Linux
* [kvm.tex](https://gitlab.com/starknet/starkbrain/-/blob/main/linux/kvm.tex) : Tutoriel sur l'installation de KVM
