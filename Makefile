# Paquets LaTeX requis
LATEX_PACKAGES = texlive texlive-lang-french texlive-latex-extra

# Répertoire des fichiers LaTeX
TEX_DIR = linux

# Commande de compilation LaTeX
LATEX_COMPILER = pdflatex

# Options de compilation
LATEX_FLAGS = -interaction=nonstopmode -halt-on-error

# Obtention de la liste de tous les fichiers LaTeX
TEX_FILES := $(wildcard $(TEX_DIR)/*.tex)
ROOT_TEX_FILES := $(wildcard ./*.tex)

# Conversion des fichiers en PDF
PDF_FILES := $(patsubst $(TEX_DIR)/%.tex, $(TEX_DIR)/%.pdf, $(TEX_FILES)) $(patsubst %.tex, %.pdf, $(ROOT_TEX_FILES))

all: $(PDF_FILES)

# Deux passes de compilation nécessaires pour générer la table des matières
$(TEX_DIR)/%.pdf: $(TEX_DIR)/%.tex
	$(LATEX_COMPILER) $(LATEX_FLAGS) -output-directory=$(TEX_DIR) $<
	$(LATEX_COMPILER) $(LATEX_FLAGS) -output-directory=$(TEX_DIR) $<

%.pdf: %.tex
	$(LATEX_COMPILER) $(LATEX_FLAGS) $<
	$(LATEX_COMPILER) $(LATEX_FLAGS) $<

# Supprime tous les fichiers générés
clean:
	rm -f $(PDF_FILES) $(TEX_DIR)/*.aux $(TEX_DIR)/*.log $(TEX_DIR)/*.out $(TEX_DIR)/*.toc
	rm -f ./*.aux ./*.log ./*.out ./*.toc

# Sauf les PDF
clean-aux:
	rm -f $(TEX_DIR)/*.aux $(TEX_DIR)/*.log $(TEX_DIR)/*.out $(TEX_DIR)/*.toc
	rm -f ./*.aux ./*.log ./*.out ./*.toc

# Ne génère que les PDF
pdf: all clean-aux

# Installe les paquets LaTeX nécessaires pour la compilation
install:
	apt update
	apt install $(LATEX_PACKAGES) -y

# Les désinstalle
uninstall:
	apt remove $(LATEX_PACKAGES) -y

.PHONY: all clean clean-aux pdf install uninstall
